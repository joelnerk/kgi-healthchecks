#!/usr/bin/env python

ServerData = {
    '_key': '',
    'ServerName': '',
    'CPUCount': '',
    'TotalMem': '',
    'OSName': '',
    'SplunkVersion': '',
    'MaxUsers':'',
    'ActiveLicenseGroup': '',
    'MaxUsers': '',
    'HTTPPort': '',
    'SSLEnabled': '',
    'WebServerEnabled': '',
    'ServerStatus': '',
    'LicenseKeys': ''
}

