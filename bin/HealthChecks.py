#!/usr/bin/env python

import os
import sys
import ast
import time
import json
import logging
import logging.handlers
import requests

import ServerData

from xml.etree import ElementTree
from configparser import ConfigParser


LOG_LEVEL = logging.DEBUG
LOG_FILE_NAME = "auiRAD-MSP.log"

logger = object()

def setup_logging(parser):
    global SPLUNK_HOME, LOG_LEVEL, LOG_FILE_NAME, logger
    if 'SPLUNK_HOME' in os.environ:
        SPLUNK_HOME = os.environ['SPLUNK_HOME']
    else:
        SPLUNK_HOME = parser.get('config','SPLUNK_HOME')

    LOG_FILE_NAME = parser.get('config','LOG_FILE_NAME')

    log_format = "%(asctime)s %(levelname)-s\t%(module)s[%(process)d]:%(lineno)d - %(message)s"
    logger = logging.getLogger('v')

    logLevel = parser.get('config','LOG_LEVEL')

    if logLevel == 'DEBUG':
        LOG_LEVEL = logging.DEBUG
    elif logLevel == 'INFO':
        LOG_LEVEL = logging.INFO
    elif logLevel == 'WARNING':
        LOG_LEVEL = logging.WARNING
    elif logLevel == 'ERROR':
        LOG_LEVEL = logging.ERROR
    elif logLevel == 'CRITICAL':
        LOG_LEVEL = logging.CRITICAL
    else:
        LOG_LEVEL = logging.DEBUG

    logger.setLevel(LOG_LEVEL)

    l = logging.handlers.RotatingFileHandler(os.path.join(SPLUNK_HOME, 'var', 'log', 'splunk', LOG_FILE_NAME), mode='a', maxBytes = 1000000, backupCount=2)
    l.setFormatter(logging.Formatter(log_format))
    logger.addHandler(l)

    logger.propagate = False


def config():
    parser = ConfigParser()
    parser.read('queries.conf')

    setup_logging(parser)

    return parser


def getKVStoreData():
    servers = ast.literal_eval(parser.get('config', 'SERVERS'))

    for server in servers:
        host = server['host']
        logger.debug('host:' + host)

        for serverType in server['serverTypes']:
            logger.debug(' serverType: ' + serverType)

    return servers


def putKVStoreData(parser):

    if parser.get('config','kvstorePut') == 'false':
        return

    host = parser.get('config','kvstore')
    uid = parser.get('config','uid')
    pwd = parser.get('config','pwd')

    uriInsert = 'https://' + host + ':8089/servicesNS/nobody/kgiHealthChecks/storage/collections/data/KGI_ServerData'
    uriUpdate = 'https://' + host + ':8089/servicesNS/nobody/kgiHealthChecks/storage/collections/data/KGI_ServerData' + '/' + ServerData.ServerData['_key']

    headers = {'content-type': 'application/json'}
    params = {}
    data   = json.dumps(ServerData.ServerData)

    response = requests.post(uriUpdate, params=params, data=data, headers=headers, verify=False, auth=(uid, pwd))
    logger.debug('kvstore uri: update ' + uriUpdate)
    logger.debug('uriUpdate putKVStore: ' + str(response.text))
    if str(response.text).find('<msg type="ERROR">Could not find object.</msg>') != -1:
        response = requests.post(uriInsert, params=params, data=data, headers=headers, verify=False, auth=(uid, pwd))
        logger.debug('kvstore uri: insert ' + uriInsert)
        logger.debug('uriInsert putKVStore: ' + str(response.text))


def setKVStoreFields(response):

    tree = ElementTree.fromstring(str(response))

    #
    # Set Data Object for this server
    #

    node = tree.find('.//*[@name="serverName"]')
    if node is not None:
        ServerData.ServerData['ServerName'] = node.text

    node = tree.find('.//*[@name="cpu_count"]')
    if node is not None:
        ServerData.ServerData['CPUCount'] = node.text

    node = tree.find('.//*[@name="physicalMemoryMB"]')
    if node is not None:
        ServerData.ServerData['TotalMem'] = node.text

    node = tree.find('.//*[@name="os_name"]')
    if node is not None:
        ServerData.ServerData['OSName'] = node.text

    node = tree.find('.//*[@name="splunk_version"]')
    if node is not None:
        ServerData.ServerData['SplunkVersion'] = node.text

    node = tree.find('.//*[@name="max_users"]')
    if node is not None:
        ServerData.ServerData['MaxUsers'] = node.text

    node = tree.find('.//*[@name="activeLicenseGroup"]')
    if node is not None:
        ServerData.ServerData['ActiveLicenseGroup'] = node.text

    node = tree.find('.//*[@name="httpport"]')
    if node is not None:
        ServerData.ServerData['HTTPPort'] = node.text

    node = tree.find('.//*[@name="enableSplunkWebSSL"]')
    if node is not None:
        ServerData.ServerData['SSLEnabled'] = node.text

    node = tree.find('.//*[@name="startwebserver"]')
    if node is not None:
        ServerData.ServerData['WebServerEnabled'] = node.text

    node = tree.find('.//*[@name="licenseKeys"]')
    if node is not None:
        for list in node:
            licenseKeys = ''
            for items in list:
                licenseKeys = licenseKeys + items.text + ','
                licenseKeys = licenseKeys[:-1]

        ServerData.ServerData['LicenseKeys'] = licenseKeys

    ServerData.ServerData['ServerStatus'] = 'up'


def putOutputDirData(parser, host, section_name, response):

    if parser.get('config','outputDirPut') == 'false':
        return

    filePath = parser.get('config','outputDir')
    logger.debug('filePath:' + filePath)

    fullPath = filePath + os.sep + host + os.sep
    logger.debug('fullPath:' + fullPath)

    if not os.path.exists(fullPath):
        os.makedirs(fullPath)

    text_file = open(fullPath + section_name + '.' + parser.get(section_name, 'outputType'), "w")
    text_file.write(str(response.text))
    text_file.close()


def serverTypeValidate(parser, section_name, serverTypes):
    #
    # Validate this query should be run for this server type
    #
    sectionServerType = parser.get(section_name, 'serverTypes')

    for serverType in serverTypes:
        logger.debug('validating if call should be made for serverType: ' + serverType)
        if sectionServerType.find(serverType) != -1:
            logger.debug('  serverType: ' + serverType + ' found')
            return 'true'

    return 'false'


def restCall(host, serverTypes, parser, section_name, spl):

    logger.debug('restCall - check if this query should be run for this server type')
    if serverTypeValidate(parser, section_name, serverTypes) == 'false':
        logger.warning('restCall - skipping ' + section_name + ' invalid serverType')
        return

    uid = parser.get('config','uid')
    pwd = parser.get('config','pwd')

    uri = 'https://' + host + ':8089' + spl
    logger.debug('rest  uri:' + uri)

    response = requests.get(uri, verify=False, auth=(uid, pwd))
    putOutputDirData(parser, host, section_name, response)

    setKVStoreFields(response.text)

    logger.debug('rest job: ' +str(response.text))



def splCall(host, serverTypes, parser, section_name, spl):

    logger.debug('splCall - check if this query should be run for this server type')
    if serverTypeValidate(parser, section_name, serverTypes) == 'false':
        logger.warning('splCall - skipping ' + section_name + ' invalid serverType')
        return

    uid = parser.get('config','uid')
    pwd = parser.get('config','pwd')

    #
    # Submit the search job and get its jobid
    #
    uri = 'https://' + host + ':8089/services/search/jobs'
    logger.debug('spl job search uri:' + uri)

    params = {'search': 'search ' + spl}

    response = requests.post(uri, data=params, verify=False, auth=(uid, pwd))
    tree = ElementTree.fromstring(str(response.text))
    node = tree.find('sid')
    if node is None:
        logger.debug(str(response.text))
        return
    else:
        jobid = node.text

    logger.debug('spl search: ' +str(response.text))

    #
    # Query the jobid to get the status
    #
    uri = 'https://' + host + ':8089/services/search/jobs/' + jobid
    logger.debug('spl job id uri:' + uri)

    isNotDone = True
    while isNotDone:
        response = requests.post(uri, verify=False, auth=(uid, pwd))
        tree = ElementTree.fromstring(str(response.text))
        node = tree.find('.//*[@name="isDone"]')
        status = node.text

        if status == '1':
            isNotDone = False
        else:
            time.sleep(1)
            logger.debug('spl job processing, retrying')

    logger.debug('spl job: ' +str(response.text))

    #
    # Submit the search job and get its jobid
    #
    uri = 'https://' + host + ':8089/services/search/jobs/' + jobid + '/results?output_node=' + parser.get(section_name, 'outputType')
    logger.debug('spl job response uri:' + uri)

    response = requests.get(uri, verify=False, auth=(uid, pwd))
    logger.debug('spl results: ' +str(response.text))

    putOutputDirData(parser, host, section_name, response)



def main(servers, parser):

    for server in servers:
        host = server['host']
        logger.debug('host:' + host)

        #
        # Init Data Object for this server
        #
        ServerData.ServerData['_key']               = host
        ServerData.ServerData['ServerName']         = ''
        ServerData.ServerData['CPUCount']           = ''
        ServerData.ServerData['TotalMem']           = ''
        ServerData.ServerData['OSName']             = ''
        ServerData.ServerData['SplunkVersion']      = ''
        ServerData.ServerData['MaxUsers']           = ''
        ServerData.ServerData['ActiveLicenseGroup'] = ''
        ServerData.ServerData['HTTPPort']           = ''
        ServerData.ServerData['SSLEnabled']         = ''
        ServerData.ServerData['WebServerEnabled']   = ''
        ServerData.ServerData['ServerStatus']       = ''
        ServerData.ServerData['LicenseKeys']        = ''

        for section_name in parser.sections():
            logger.debug('query processing:' + section_name)

            if section_name == 'config':
                continue

            with open(section_name + '.spl', 'r') as splFile:
                spl = splFile.read().replace('\n', '')


            if parser.get(section_name, 'queryType') == 'rest':
                restCall(host, server['serverTypes'], parser, section_name, spl)
                continue

            if parser.get(section_name, 'queryType') == 'spl':
                splCall(host, server['serverTypes'], parser, section_name, spl)
                continue

        putKVStoreData(parser)


if __name__ == '__main__':

    parser = config()

    logger.debug('**** Start of Run ****')

    hosts  = getKVStoreData()

    main(hosts, parser)

    logger.debug('**** End of Run ****')

    sys.exit()

